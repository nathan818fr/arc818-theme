#!/bin/sh
#
# Script that copies (or overwrites) personal assets.
# It is used in meson.build configurations.
#
# Note: Assets have to be rendered before (with render_assets.sh).
#

set -eu

src_dir="$(dirname "$(readlink -f "$0")")/render"
dst_dir="$(dirname "$(readlink -f "$1")")"
cp -TR -- "$src_dir" "$dst_dir"

exit 0
