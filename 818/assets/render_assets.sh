#!/bin/sh
#
# Script that renders personal assets.
# Assets whose IDs are listed in assets.txt are rendered from assets.svg (to PNG with multiple DPIs).
#
# Note: Some assets are copied directly from the pixel-perfect directory if they exist.
#

set -eu
cd "$(dirname "$(readlink -f "$0")")"

assets_list='assets.txt'
assets_file='assets.svg'
render_dir='render'

if [ "${1:-}" = 'clean' ]; then
  rm -rf "$render_dir"
  exit 0
fi

mkdir -p "$render_dir"
while IFS= read -r asset_id; do
  for dpi in '96;' '192;@2'; do
    dpi_value="${dpi%%;*}"
    dpi_suffix="${dpi##*;}"

    filename="${asset_id}${dpi_suffix}.png"
    output_file="${render_dir}/${filename}"
    pp_file="pixel-perfect/${filename}"

    echo "$filename ..."
    if [ -f "$output_file" ]; then
      echo " '$output_file' already exists"
    elif [ -f "$pp_file" ]; then
      echo " using pixel-perfect file '${pp_file}'"
      cp -- "$pp_file" "$output_file"
    else
      inkscape \
        --export-id-only \
        --export-filename="$output_file" \
        --export-id="$asset_id" \
        --export-dpi="$dpi_value" \
        -- "$assets_file" 2>&1 |
        awk '{print " "$0}'
    fi
  done
done <"$assets_list"

echo 'Done'
exit 0
