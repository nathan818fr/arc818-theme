#!/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit
cd "$(dirname "$(readlink -f "$0")")"

function main() {
  build_dir="$(pwd)/build"
  out_dir="${build_dir}/out"

  case "${1:-}" in
  clean) clean "$@" ;;
  build) build "$@" ;;
  package) package "$@" ;;
  install) install "$@" ;;
  *)
    echo "Usage: $0 clean|build|package|install" >&2
    exit 1
    ;;
  esac
}

function clean() {
  ./assets/render_assets.sh clean
  rm -rf "$build_dir"
}

function build() {
  case "${_818_BUILD_TYPE:-}" in
  release)
    CINNAMON_VERSION=4.8
    GNOME_SHELL_VERSION=40
    GTK3_VERSION=3.24
    ;;
  *) ;;
  esac

  themes='gtk2,gtk3,metacity,plank,unity,xfwm'
  if [[ -n "${CINNAMON_VERSION:-}" ]] || type -f cinnamon >/dev/null 2>&1; then
    themes+=',cinnamon'
  fi
  if [[ -n "${GNOME_SHELL_VERSION:-}" ]] || type -f gnome-shell >/dev/null 2>&1; then
    themes+=',gnome-shell'
  fi

  meson_conf=(--prefix="${out_dir}" -Dthemes="$themes" -Dtransparency=false)
  if [[ -n "${CINNAMON_VERSION:-}" ]]; then
    meson_conf+=(-Dcinnamon_version="$CINNAMON_VERSION")
  fi
  if [[ -n "${GNOME_SHELL_VERSION:-}" ]]; then
    meson_conf+=(-Dgnome_shell_version="$GNOME_SHELL_VERSION")
  fi
  if [[ -n "${GTK3_VERSION:-}" ]]; then
    meson_conf+=(-Dgtk3_version="$GTK3_VERSION")
  fi

  if [[ ! -d "$build_dir" ]]; then
    meson setup "${meson_conf[@]}" "$build_dir" ../
  else
    meson configure "${meson_conf[@]}" "$build_dir"
  fi

  ./assets/render_assets.sh
  meson compile -C "$build_dir"
  meson install -C "$build_dir"
}

function package() {
  tar -cv -C "${out_dir}/share/themes" --transform 's,^\.,Arc818-theme,' . | gzip >"Arc818-theme.tar.gz"
}

function install() {
  while IFS= read -r -d '' file; do
    dst="/usr/share/themes/$(basename "$file")"
    set -x
    sudo rm -rf "$dst"
    sudo cp -TR "$file" "$dst"
    { set +x; } 2>/dev/null
  done < <(find "${out_dir}/share/themes" -mindepth 1 -maxdepth 1 -type d -print0)

  echo 'Done'
}

main "$@"
exit "$?"
